package ovbg.razortechapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;


/**
 * Created by OfirBg on 18/08/2016.
 */
    public class Downloader {

    private static final String TAG = "Downloader";


    public static void getJsonObject(Context ctx, String url, final onDownloaderResponse listener){

        final ProgressDialog pDialog = new ProgressDialog(ctx);
        pDialog.setMessage("Loading...");
        pDialog.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();
                        listener.onSuccess(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                listener.onFailed(error);
                // hide the progress dialog
                pDialog.hide();
            }
        });

// Adding request to request queue
        RazorTechApp.getInstance().addToRequestQueue(jsonObjReq);
    }


    public interface onDownloaderResponse{
        JSONObject onSuccess(JSONObject response);
        JSONObject onFailed(Exception ex);
    }


}
