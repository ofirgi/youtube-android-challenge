package ovbg.razortechapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends Activity {
    private ExpandableListView expListView;
    private ArrayList<String> mListTitles;
    private HashMap<String, List<JSONObject>> mDataChild;
    private YouTubeExpandableAdapter mListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        String url = "http://www.razor-tech.co.il/hiring/youtube-api.json";
        Downloader.getJsonObject(this, url, new Downloader.onDownloaderResponse() {
            @Override
            public JSONObject onSuccess(JSONObject response) {

                if (prepareData(response)){
                    mListAdapter = new YouTubeExpandableAdapter(MainActivity.this, mListTitles, mDataChild);
                    expListView.setAdapter(mListAdapter);
                }
                else{

                }
                return null;


            }

            @Override
            public JSONObject onFailed(Exception ex) {
                return null;
            }
        });

    }

    private boolean prepareData(JSONObject listData){
        boolean success = false;
        mListTitles = new ArrayList<>();
        mDataChild = new HashMap<String, List<JSONObject>>();
        try {
            JSONArray playLists = listData.getJSONArray("Playlists");
            for (int i = 0; i < playLists.length(); i++){
                mListTitles.add(playLists.getJSONObject(i).getString("ListTitle"));
                JSONArray childList = playLists.getJSONObject(i).getJSONArray("ListItems");
                List<JSONObject> childLisResult = new ArrayList<>();
                for (int x = 0; x < childList.length(); x++){
                    childLisResult.add(childList.getJSONObject(x));
                }
                mDataChild.put(playLists.getJSONObject(i).getString("ListTitle"), childLisResult);
            }
            success = !mListTitles.isEmpty() && !mDataChild.isEmpty();

        } catch (JSONException e) {
            success = false;
            e.printStackTrace();
        }

        return success;

    }


}
